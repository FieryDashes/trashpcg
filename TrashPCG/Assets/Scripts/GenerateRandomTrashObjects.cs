﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenerateRandomTrashObjects : MonoBehaviour {
    [HideInInspector] public Transform objectToSpawnOn;
    [HideInInspector] public LayerMask objectToSpawnOnLayer;
    public List<GameObject> objectsToSpawn = new List<GameObject> ();
    public List<GameObject> decalObjectsToSpawn = new List<GameObject> ();
    [HideInInspector] public float trashAmountToSpawn = 10;
    [HideInInspector] public float decalAmountToSpawn = 3;
    [HideInInspector] public float ySpawnOffest = 1;
    [HideInInspector] public Transform spawnedTrashTransform;
    [HideInInspector] public float trashMinYHeight, trashMaxYHeight;
    [HideInInspector] public float decalYHeight;
    float tempAmount;

    MapGenerator mapGenerator;

    public void SpawnRandomTrash (bool _randomHeight, bool _trashType) {
        //Get trash type info from enum
        /* Not necessary yet */
        //Get object to spawn on's rendered
        // var tempList = spawnedTrashTransform.Cast<Transform> ().ToList ();
        // foreach (var child in tempList) {
        //     DestroyImmediate (child.gameObject);
        // }

        Renderer renderer = objectToSpawnOn.GetComponent<Renderer> ();
        if (_trashType) {
            tempAmount = trashAmountToSpawn;
        } else {
            tempAmount = decalAmountToSpawn;
        }
        for (int i = 0; i < tempAmount; i++) {
            //Get random position on mesh
            float x = Random.Range (renderer.bounds.min.x, renderer.bounds.max.x);
            float z = Random.Range (renderer.bounds.min.z, renderer.bounds.max.z);
            //Get mesh max height through renderer.bounds
            float maxHeight = renderer.bounds.max.y + 10;
            //Raycast down from just above that
            RaycastHit hit;
            if (Physics.Raycast (new Vector3 (x, maxHeight, z), -Vector3.up, out hit, Mathf.Infinity, objectToSpawnOnLayer)) {
                float tempTrashHeight;

                if (_randomHeight) {
                    tempTrashHeight = Random.Range (trashMinYHeight, trashMaxYHeight);
                } else {
                    tempTrashHeight = ySpawnOffest;
                }

                if (_trashType) {
                    GameObject newTrash = Instantiate (objectsToSpawn[Random.Range (0, objectsToSpawn.Count)],
                        hit.point + new Vector3 (0, tempTrashHeight, 0), Quaternion.LookRotation (hit.normal), spawnedTrashTransform);
                    Debug.Log ("Spawning Trash");
                } else {
                    GameObject newDecal = Instantiate (decalObjectsToSpawn[Random.Range (0, decalObjectsToSpawn.Count)],
                        hit.point + new Vector3 (0, decalYHeight, 0), Quaternion.LookRotation (hit.normal), spawnedTrashTransform);
                    Debug.Log ("Spawning Decals");
                }
            }
        }
    }

    public void RemoveAllTrash () {
        var tempList = spawnedTrashTransform.Cast<Transform> ().ToList ();
        foreach (var child in tempList) {
            DestroyImmediate (child.gameObject);
        }
    }
}