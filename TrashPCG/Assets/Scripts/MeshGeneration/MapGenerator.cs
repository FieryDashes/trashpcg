﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

	//public enum DrawMode { NoiseMap, ColourMap, Mesh }

	//public enum RubbishType { Plastic, Metal, Glass, Paper } // Types of trash it can make procedurally

	//public DrawMode drawMode;
	//[Space (10)]
	//public RubbishType rubbishType;
	const int mapChunkSize = 241;
	[Range (0, 6)]
	[HideInInspector] public int levelOfDetail = 5;
	[Range (150, 350)]

	[Header ("Mesh Generation Settings")]

	public float noiseScale;

	[HideInInspector] public int octaves = 4;
	[Range (0.5f, 0f)]
	public float smoothness;
	[HideInInspector] public float lacunarity = 2;
	public int seed;
	public Vector2 offset;

	public bool useFalloff;
	float[, ] falloffMap;

	public float meshHeightMultiplier;
	public AnimationCurve meshHeightCurve;
	[Range (2, 4)]
	[SerializeField] float meshVolume = 3f;
	[Range (1, 2)]
	[SerializeField] float meshFootprint = 2.2f;

	[Header ("Other")]

	public bool autoUpdate;

	public TerrainType[] regions; //Maybe have the regions also mask, so the bottom region is an alpha mask
	[Tooltip ("Requires reference to mesh that will be manipulated")]
	[SerializeField] GameObject targetBaseMesh;
	MeshCollider meshCollider;
	GenerateRandomTrashObjects generateRandomTrashObjects;

	[Header ("Trash Spawning Settings")]
	[SerializeField] Transform trashSpawnLocation;
	[SerializeField] List<GameObject> trashObjectsToSpawn = new List<GameObject> ();
	[SerializeField] List<GameObject> decalObjectsToSpawn = new List<GameObject> ();
	[SerializeField] LayerMask trashSpawnLayer;
	[SerializeField] float trashSpawnAmount = 10;
	[SerializeField] float decalSpawnAmount = 3;

	[Header ("Final Trash Creation")]
	public GameObject trashToConvert;

	void Awake () {
		meshCollider = targetBaseMesh.GetComponent<MeshCollider> ();
		falloffMap = FalloffGenerator.GenerateFalloffMap (mapChunkSize, meshVolume, meshFootprint);
		generateRandomTrashObjects = GetComponent<GenerateRandomTrashObjects> ();
	}

	public void GenerateRandomMap () {
		seed = UnityEngine.Random.Range (-999999, 999999);
		offset = new Vector2 (UnityEngine.Random.Range (-9999, 9999), UnityEngine.Random.Range (-9999, 9999));
		noiseScale = UnityEngine.Random.Range (225, 275);
		meshHeightMultiplier = UnityEngine.Random.Range (50, 100);
		smoothness = UnityEngine.Random.Range (0.45f, 0.25f);
		meshVolume = UnityEngine.Random.Range (2f, 4f);
		meshFootprint = UnityEngine.Random.Range (1f, 2f);

		GenerateMap ();
	}
	public void GenerateDefaultMap () {
		seed = 0;
		offset = new Vector2 (0, 0);
		noiseScale = 250;
		meshHeightMultiplier = 135;
		smoothness = 0.4f;

		GenerateMap ();
	}

	public void GenerateMap () {

		if (targetBaseMesh == null) {
			Debug.LogError ("TargetBaseMesh is null!", targetBaseMesh);
			return;
		}

		float[, ] noiseMap = Noise.GenerateNoiseMap (mapChunkSize, mapChunkSize, seed, noiseScale, octaves, smoothness, lacunarity, offset);

		Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
		for (int y = 0; y < mapChunkSize; y++) { //In y axis for the map chunk size
			for (int x = 0; x < mapChunkSize; x++) { //In x axis for the map chunk size
				if (useFalloff) {
					noiseMap[x, y] = Mathf.Clamp01 (noiseMap[x, y] - falloffMap[x, y]);
				}
				float currentHeight = noiseMap[x, y];

				for (int i = 0; i < regions.Length; i++) {
					if (currentHeight <= regions[i].height) {
						colourMap[y * mapChunkSize + x] = regions[i].colour;
						break;
					}
				}
			}
		}

		MapDisplay display = FindObjectOfType<MapDisplay> ();
		display.DrawMesh (MeshGenerator.GenerateTerrainMesh (noiseMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail),
			TextureGenerator.TextureFromColourMap (colourMap, mapChunkSize, mapChunkSize));

		for (int i = 0; i < 3; i++) {
			ResetMeshCollider ();
		}
	}

	public void ResetMeshCollider () {
		DestroyImmediate (meshCollider);
		targetBaseMesh.AddComponent<MeshCollider> ();
		meshCollider = targetBaseMesh.GetComponent<MeshCollider> ();
	}

	public void InstantiateTrashFinal () {

		//GameObject trashObject = Instantiate (trashToConvert);
	}

	void OnValidate () {
		if (lacunarity < 1) {
			lacunarity = 1;
		}

		if (octaves < 0) {
			octaves = 0;
		}

		falloffMap = FalloffGenerator.GenerateFalloffMap (mapChunkSize, meshVolume, meshFootprint);

		generateRandomTrashObjects = GetComponent<GenerateRandomTrashObjects> ();

		generateRandomTrashObjects.objectToSpawnOn = targetBaseMesh.transform;
		generateRandomTrashObjects.objectToSpawnOnLayer = trashSpawnLayer;
		generateRandomTrashObjects.objectsToSpawn.AddRange (trashObjectsToSpawn);
		generateRandomTrashObjects.decalObjectsToSpawn.AddRange (decalObjectsToSpawn);
		generateRandomTrashObjects.trashAmountToSpawn = trashSpawnAmount;
		generateRandomTrashObjects.decalAmountToSpawn = decalSpawnAmount;
		generateRandomTrashObjects.spawnedTrashTransform = trashSpawnLocation;
	}
}

[System.Serializable]
public struct TerrainType {
	public string name;
	public float height;
	public Color colour;
}

//Affect sides of mesh to always be pinned to the lowest point (Kinda Done?) Needs to be linear
//Generate textures based on textures provided | Generate in non-tiled manner, procedurally | per height (No Time for now)
//Generate decals randomly over mesh representing bottles or trash in general (No Time for now)
//Spawn particles of mesh randomly over mesh | Also bottles or random trash