﻿using System.Collections;
using UnityEngine;

public static class Noise {
	// static float SampleFalloff () {
	// 	Vector2 worldPoint;
	// 	worldPoint /= mapSize;

	// 	Vector2 relationPoint = worldPoint * falloffSize;
	// 	float falloffSample = falloffTexture.GetPixel (relativePoint);
	// 	return falloffSample;
	// }

	public static float[, ] GenerateNoiseMap (int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset /* , Texture2D falloffTexture, float falloffSize */ ) {
		float[, ] noiseMap = new float[mapWidth, mapHeight];

		System.Random prng = new System.Random (seed);
		Vector2[] octaveOffsets = new Vector2[octaves];
		for (int i = 0; i < octaves; i++) {
			float offsetX = prng.Next (-100000, 100000) + offset.x;
			float offsetY = prng.Next (-100000, 100000) + offset.y;
			octaveOffsets[i] = new Vector2 (offsetX, offsetY);
		}

		if (scale <= 0) {
			scale = 0.0001f;
		}

		float maxNoiseHeight = float.MinValue;
		float minNoiseHeight = float.MaxValue;

		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {

				float amplitude = 1;
				float frequency = 1;
				float noiseHeight = 0;

				for (int i = 0; i < octaves; i++) {
					float sampleX = (x - halfWidth) / scale * frequency + octaveOffsets[i].x;
					float sampleY = (y - halfHeight) / scale * frequency + octaveOffsets[i].y;

					/* Vector2 worldPoint = new Vector2 (sampleX, sampleY) / 241;
					Vector2 relationPoint = worldPoint * falloffSize;
					float falloffSample = falloffTexture.GetPixel ((int) relationPoint.x, (int) relationPoint.y).grayscale; */

					// float rSqaured = sampleX * sampleX + sampleY * sampleY;
					// float attenuation = 1f / rSqaured;

					//float falloffFactor = FallOffFactor (x, y, sampleX, sampleY);

					float perlinValue = Mathf.PerlinNoise (sampleX, sampleY) * 2 - 1;
					//perlinValue = perlinValue / falloffFactor;

					noiseHeight += perlinValue * amplitude; //NoiseHeight is the final output from the perlin noise

					amplitude *= persistance;
					frequency *= lacunarity;
				}

				if (noiseHeight > maxNoiseHeight) {
					maxNoiseHeight = noiseHeight;
				} else if (noiseHeight < minNoiseHeight) {
					minNoiseHeight = noiseHeight;
				}

				noiseMap[x, y] = noiseHeight;
			}
		}

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				noiseMap[x, y] = Mathf.InverseLerp (minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
			}
		}
		return noiseMap;
	}
	// static float FallOffFactor (int x, int y, float falloffPower, float falloffRange) {
	// 	float falloffFactor = 1;
	// 	float centerX = 241 / 2;
	// 	float centerY = 241 / 2;

	// 	float distanceFromCenter = Mathf.Sqrt ((Mathf.Pow (((float) x - centerX), 2) + Mathf.Pow (((float) y - centerY), 2)));
	// 	falloffFactor = Mathf.Pow (2, (Mathf.Pow (falloffPower, (distanceFromCenter / falloffRange)) + 1));

	// 	return falloffFactor;
	// }
}

//To Do: Add falloff texture to noise being generated