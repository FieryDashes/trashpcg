﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMaskedNoise : MonoBehaviour {
    //     public Transform mergeInput; // a transform with a bunch of SpriteRenderers you want to merge

    //     [SerializeField] Texture2D maskTexture;

    //     /* Takes a transform holding many sprites as input and creates one flattened sprite out of them */
    //     public Sprite CombineWithMask (Vector2Int size, Transform input) {
    //         var spriteRenderers = input.GetComponentsInChildren<SpriteRenderer> ();
    //         if (spriteRenderers.Length == 0) {
    //             Debug.Log ("No SpriteRenderers found in " + input.name + " for SpriteMerge");
    //             return null;
    //         }

    //         Texture2D 
    //         targetTexture.filterMode = FilterMode.Point;
    //         var targetPixels = targetTexture.GetPixels ();
    //         for (int i = 0; i < targetPixels.Length; i++) targetPixels[i] = Color.clear; // default pixels are not set
    //         var targetWidth = targetTexture.width;

    //         for (int i = 0; i < spriteRenderers.Length; i++) {
    //             var sr = spriteRenderers[i];
    //             var position = (Vector2) sr.transform.localPosition - sr.sprite.pivot;
    //             var p = new Vector2Int ((int) position.x, (int) position.y);
    //             var sourceWidth = sr.sprite.texture.width;
    //             // if read/write is not enabled on texture (under Advanced) then this next line throws an error
    //             // no way to check this without Try/Catch :(
    //             var sourcePixels = sr.sprite.texture.GetPixels ();
    //             for (int j = 0; j < sourcePixels.Length; j++) {
    //                 var source = sourcePixels[j];
    //                 var x = (j % sourceWidth) + p.x;
    //                 var y = (j / sourceWidth) + p.y;
    //                 var index = x + y * targetWidth;
    //                 if (index > 0 && index < targetPixels.Length) {
    //                     var target = targetPixels[index];
    //                     if (target.a > 0) {
    //                         // alpha blend when we've already written to the target
    //                         float sourceAlpha = source.a;
    //                         float invSourceAlpha = 1f - source.a;
    //                         float alpha = sourceAlpha + invSourceAlpha * target.a;
    //                         Color result = (source * sourceAlpha + target * target.a * invSourceAlpha) / alpha;
    //                         result.a = alpha;
    //                         source = result;
    //                     }
    //                     targetPixels[index] = source;
    //                 }
    //             }
    //         }

    //         targetTexture.SetPixels (targetPixels);
    //         targetTexture.Apply (false, true); // read/write is disabled in 2nd param to free up memory
    //         return Sprite.Create (targetTexture, new Rect (new Vector2 (), size), new Vector2 (), 1, 0, SpriteMeshType.FullRect);
    //     }
    // }

    // SkinnedMeshRenderer skinnedMeshRenderer;
    // Material skinnedMeshMaterial;

    [SerializeField] Texture2D textureMask;
    [SerializeField] Rect textureMaskRect;

    public Texture2D MakeMaskedNoise (Texture2D _texture, int _width, int _height) {
        int x = Mathf.FloorToInt (textureMaskRect.x);
        int y = Mathf.FloorToInt (textureMaskRect.y);
        int width = Mathf.FloorToInt (textureMaskRect.width);
        int height = Mathf.FloorToInt (textureMaskRect.height);
        Color[] colors1 = _texture.GetPixels (x, y, _width, _height);
        Color[] colors2 = textureMask.GetPixels (x, y, _width, _height);
        int numColors = colors1.Length;
        // creates a new array the same length as colors1
        Color[] colorsCombined = new Color[numColors];
        // iterate over all elements of the arrays and multiply the color values in both color1 and color2
        for (int i = 0; i < numColors; i++)
            colorsCombined[i] = colors1[i] * colors2[i];

        //Create new texture & set
        Texture2D transparencyCombinedTexture;
        transparencyCombinedTexture = new Texture2D (width, height);
        transparencyCombinedTexture.filterMode = FilterMode.Point;
        transparencyCombinedTexture.wrapMode = TextureWrapMode.Clamp;
        transparencyCombinedTexture.SetPixels (colorsCombined);
        transparencyCombinedTexture.Apply ();
        // skinnedMeshMaterial.EnableKeyword ("_MainTex");
        // skinnedMeshMaterial.SetTexture ("_MainTex", transparencyCombinedTexture);

        return transparencyCombinedTexture;

    }
}