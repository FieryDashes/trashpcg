﻿using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (MapGenerator))]
public class MapGeneratorEditor : Editor {

	public override void OnInspectorGUI () {
		MapGenerator mapGen = (MapGenerator) target;
		GenerateRandomTrashObjects generateRandomTrashObjects = FindObjectOfType<GenerateRandomTrashObjects> ();

		if (DrawDefaultInspector ()) {
			if (mapGen.autoUpdate) {
				mapGen.GenerateMap ();
			}
		}
		GUILayout.Space (10f);
		EditorGUILayout.BeginFoldoutHeaderGroup (true, "Generate Mesh");
		GUILayout.BeginVertical ();
		if (GUILayout.Button ("From Parameters", GUILayout.Height (20))) {
			mapGen.GenerateMap ();
		}
		GUILayout.Space (1f);
		if (GUILayout.Button ("From Random Values", GUILayout.Height (20))) {
			generateRandomTrashObjects.RemoveAllTrash ();
			mapGen.GenerateRandomMap ();
		}
		GUILayout.Space (1f);
		if (GUILayout.Button ("Reset To Default", GUILayout.Height (20))) {
			generateRandomTrashObjects.RemoveAllTrash ();
			mapGen.GenerateDefaultMap ();
		}
		EditorGUILayout.EndFoldoutHeaderGroup ();
		GUILayout.Space (10f);

		EditorGUILayout.BeginFoldoutHeaderGroup (true, "Generate Trash");
		EditorGUILayout.BeginHorizontal ();
		generateRandomTrashObjects.ySpawnOffest = EditorGUILayout.FloatField ("Trash Y Offset: ", generateRandomTrashObjects.ySpawnOffest);
		if (GUILayout.Button ("Reset Offset", GUILayout.Height (20))) {
			generateRandomTrashObjects.ySpawnOffest = 1;
			mapGen.ResetMeshCollider ();
			generateRandomTrashObjects.SpawnRandomTrash (false, true);
		}
		EditorGUILayout.EndHorizontal ();
		if (GUILayout.Button ("Add (Specified Height)", GUILayout.Height (20))) {
			mapGen.ResetMeshCollider ();
			generateRandomTrashObjects.SpawnRandomTrash (false, true);
		}
		GUILayout.Space (1f);
		//EditorGUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Add (Random Height)", GUILayout.Height (20))) {
			mapGen.ResetMeshCollider ();
			generateRandomTrashObjects.SpawnRandomTrash (true, true);
		}
		GUILayout.Space (1f);
		generateRandomTrashObjects.trashMinYHeight = EditorGUILayout.FloatField ("Minimum Y Trash Offset: ", generateRandomTrashObjects.trashMinYHeight);
		GUILayout.Space (1f);
		generateRandomTrashObjects.trashMaxYHeight = EditorGUILayout.FloatField ("Maximum Y Trash Offset: ", generateRandomTrashObjects.trashMaxYHeight);
		//EditorGUILayout.EndHorizontal ();
		GUILayout.Space (1f);
		if (GUILayout.Button ("Remove", GUILayout.Height (20))) {
			generateRandomTrashObjects.RemoveAllTrash ();
		}
		GUILayout.EndVertical ();
		EditorGUILayout.EndFoldoutHeaderGroup ();

		GUILayout.Space (1f);

		EditorGUILayout.BeginFoldoutHeaderGroup (true, "Generate Decals");
		if (GUILayout.Button ("Add", GUILayout.Height (20))) {
			generateRandomTrashObjects.SpawnRandomTrash (false, false);
		}
		GUILayout.Space (1f);
		generateRandomTrashObjects.decalYHeight = EditorGUILayout.FloatField ("Y Decal Offset: ", generateRandomTrashObjects.decalYHeight);
		EditorGUILayout.EndFoldoutHeaderGroup ();

		GUILayout.Space (1f);

		EditorGUILayout.BeginFoldoutHeaderGroup (true, "Generate Prefab (WIP)");
		if (GUILayout.Button ("Instantiate Trash Object", GUILayout.Height (20))) {
			//mapGen.InstantiateTrashFinal ();
			PrefabUtility.SaveAsPrefabAsset (mapGen.trashToConvert, "Assets/" + "TrashObject" + ".prefab");
		}
		EditorGUILayout.EndFoldoutHeaderGroup ();
	}
}