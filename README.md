# Welcome to TrashPCG!

This is the ReadMe for TrashPCG, a procedurally generated trash package.


# Functionality

TrashPCG is a procedural trash generation package that makes trash by just a couple of clicks. You can generate a random trash pile and add trash models and decals to it.

# Usage

To use this package, open a unity project and double-click on the unity package. Then click on import, and you will see the package in a folder in your assets. Go to the demo scene and play around with the package. There are many parameters to adjust the trash pile's shape, but for easier usage, you can click on From Random Values to get yourself a random pile of trash.

# The Why?

I've been put in charge of trash in my group game called Settler, although this package won't be used in that game. I have been working with trash for almost six months now, so it just felt right to make this.

# Credits

To make this package possible, I followed some tutorials for the main features such as generation based on a seed and other parameters and also fall-off math for mesh edges. I have Sebastian Lague to thank for his incredible tutorials, Procedural Landmass Generation video 1-5 for the generation and 11 for the fall-off generation. His channel link here: https://www.youtube.com/user/Cercopithecan.